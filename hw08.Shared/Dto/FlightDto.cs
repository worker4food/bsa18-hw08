/*
 * Imaginary airport
 *
 * Airport dispatcher API
 *
 * OpenAPI spec version: 0.0.42
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace hw08.Dto
{ 
    /// <summary>
    /// Рейс
    /// </summary>
    [DataContract]
    public partial class FlightDto : IEquatable<FlightDto>
    { 
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or Sets Source
        /// </summary>
        [Required]
        [DataMember(Name="source")]
        public string Source { get; set; }

        /// <summary>
        /// Gets or Sets DepartureDate
        /// </summary>
        [Required]
        [DataMember(Name="departureDate")]
        public DateTimeOffset? DepartureDate { get; set; }

        /// <summary>
        /// Gets or Sets Destination
        /// </summary>
        [Required]
        [DataMember(Name="destination")]
        public string Destination { get; set; }

        /// <summary>
        /// Gets or Sets ArrivalDate
        /// </summary>
        [Required]
        [DataMember(Name="arrivalDate")]
        public DateTimeOffset? ArrivalDate { get; set; }

        /// <summary>
        /// Gets or Sets Tickets
        /// </summary>
        [DataMember(Name="tickets")]
        public IEnumerable<TicketDto> Tickets { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class FlightDto {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Source: ").Append(Source).Append("\n");
            sb.Append("  DepartureDate: ").Append(DepartureDate).Append("\n");
            sb.Append("  Destination: ").Append(Destination).Append("\n");
            sb.Append("  ArrivalDate: ").Append(ArrivalDate).Append("\n");
            sb.Append("  Tickets: ").Append(Tickets).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((FlightDto)obj);
        }

        /// <summary>
        /// Returns true if FlightDto instances are equal
        /// </summary>
        /// <param name="other">Instance of FlightDto to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(FlightDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    Source == other.Source ||
                    Source != null &&
                    Source.Equals(other.Source)
                ) && 
                (
                    DepartureDate == other.DepartureDate ||
                    DepartureDate != null &&
                    DepartureDate.Equals(other.DepartureDate)
                ) && 
                (
                    Destination == other.Destination ||
                    Destination != null &&
                    Destination.Equals(other.Destination)
                ) && 
                (
                    ArrivalDate == other.ArrivalDate ||
                    ArrivalDate != null &&
                    ArrivalDate.Equals(other.ArrivalDate)
                ) && 
                (
                    Tickets == other.Tickets ||
                    Tickets != null &&
                    Tickets.SequenceEqual(other.Tickets)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (Source != null)
                    hashCode = hashCode * 59 + Source.GetHashCode();
                    if (DepartureDate != null)
                    hashCode = hashCode * 59 + DepartureDate.GetHashCode();
                    if (Destination != null)
                    hashCode = hashCode * 59 + Destination.GetHashCode();
                    if (ArrivalDate != null)
                    hashCode = hashCode * 59 + ArrivalDate.GetHashCode();
                    if (Tickets != null)
                    hashCode = hashCode * 59 + Tickets.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(FlightDto left, FlightDto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(FlightDto left, FlightDto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
