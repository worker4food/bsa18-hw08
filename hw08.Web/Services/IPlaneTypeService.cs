using hw08.Dto;
using hw08.Models;

namespace hw08.Services
{
    public interface IPlaneTypeService: IAirService<long, PlaneType, PlaneTypeDto> { }
}
