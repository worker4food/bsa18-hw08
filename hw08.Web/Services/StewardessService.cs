using hw08.Dto;
using hw08.Models;
using hw08.DAL;

namespace hw08.Services
{
    public class StewardessService : DummyAirService<long, Stewardess, StewardessDto>, IStewardessService
    {
        public StewardessService(IRepository<long, Stewardess> repo, MapperService m) : base(repo, m)
        {
        }
    }
}
