using hw08.Dto;
using hw08.Models;

namespace hw08.Services
{
    public interface IPilotService : IAirService<long, Pilot, PilotDto> {}
}
