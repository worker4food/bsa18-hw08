using System;
using hw08.Dto;
using hw08.Models;
using hw08.DAL;

namespace hw08.Services
{
    public class DepartureService : DummyAirService<long, Departure, DepartureDto>, IDepartureService
    {
        public DepartureService(IRepository<long, Departure> repo, MapperService m) : base(repo, m)
        {
        }

        public DepartureDto MakeDeparture(long id)
        {
            var d = repo.Get(id);

            if(d == null)
                throw NotFoundEx(id);

            d.DepartureDate = DateTime.Now;
            repo.Update(d);

            return ToDto(d);
        }

        public override DepartureDto CreateNew(DepartureDto item)
        {
            item.DepartureDate = default(DateTimeOffset);
            return base.CreateNew(item);
        }

        public override DepartureDto UpdateById(long id, DepartureDto item)
        {
            item.DepartureDate = default(DateTimeOffset);
            return base.CreateNew(item);
        }
    }
}
