using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using hw08.DAL;
using hw08.Dto;
using hw08.Models;

namespace hw08.Services
{
    public class CrewService : DummyAirService<long, Crew, CrewDto>, ICrewService
    {
        protected IRepository<long, CrewStewardess> crewSrtewardessRepo;
        public CrewService(
            IRepository<long, Crew> repo,
            IRepository<long, CrewStewardess> crewSrtewardessRepo,
            MapperService m) : base(repo, m)
        {
            this.crewSrtewardessRepo = crewSrtewardessRepo;
        }

        public override IEnumerable<CrewDto> GetList()
        {
            return from p in repo.GetAll()
                    .Include(x => x.CrewStewardesses)
                    .ThenInclude(x => x.Stewardess)
                    select ToDto(p);
        }

        public override CrewDto GetById(long id)
        {
            var c = repo.GetAll()
                .Include(x => x.CrewStewardesses)
                .ThenInclude(x => x.Stewardess)
                .Where(x => x.Id == id)
                .FirstOrDefault();

            if(c == null)
                throw NotFoundEx(id);

            return ToDto(c);
        }

        public IEnumerable<StewardessDto> GetStewardessList(long id) =>
            GetById(id).Stewardesses;

        public StewardessDto AddStewardess(long crewId, long id)
        {
            var newCrewStew = new CrewStewardess {
                CrewId = crewId,
                StewardessId = id
            };

            crewSrtewardessRepo.Insert(newCrewStew);

            return reverseMapper.Map<Stewardess, StewardessDto>(newCrewStew.Stewardess);
        }

        public StewardessDto RemoveStewardess(long crewId, long id)
        {
            var toRemove = crewSrtewardessRepo.GetAll()
                .Single(x => x.CrewId == crewId && x.StewardessId == id);

            crewSrtewardessRepo.Delete(toRemove.Id);

            return reverseMapper.Map<Stewardess, StewardessDto>(toRemove.Stewardess);
        }
    }
}
