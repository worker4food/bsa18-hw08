using hw08.Dto;
using hw08.Models;

namespace hw08.Services
{
    public interface IDepartureService : IAirService<long, Departure, DepartureDto>
    {
        DepartureDto MakeDeparture(long id);
    }
}
