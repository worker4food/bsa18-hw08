using System;
using System.Linq;
using hw08.Dto;
using hw08.Models;
using hw08.DAL;

namespace hw08.Services
{
    public class PlaneTypeService: DummyAirService<long, PlaneType, PlaneTypeDto>, IPlaneTypeService
    {
        public PlaneTypeService(IRepository<long, PlaneType> r, MapperService m) : base(r, m)
        {}
    }
}
