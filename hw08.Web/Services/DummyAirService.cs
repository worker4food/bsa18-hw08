using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using hw08.DAL;
using hw08.Models;

namespace hw08.Services
{
    ///<summary>
    /// Simple and straightforward service for CRUD operations
    ///</summary>
    public class DummyAirService<TKey, T, TDto> : IAirService<TKey, T, TDto>
        where T : Entity<TKey>
        //where TDto : class
    {
        protected IRepository<TKey, T> repo;
        protected IMapper directMapper;
        protected IMapper reverseMapper;

        public DummyAirService(IRepository<TKey, T> repo, MapperService m)
        {
            this.repo = repo;
            directMapper = m.DirectMapper; // from DTO
            reverseMapper = m.ReverseMapper; // to DTO
        }

        virtual public TDto ToDto(T x) =>
            reverseMapper.Map<T, TDto>(x);

        virtual public T FromDto(TDto x) =>
            directMapper.Map<TDto, T>(x);

        virtual public T FromDto(TDto src, T dest) =>
            directMapper.Map<TDto, T>(src, dest);

        virtual public IEnumerable<TDto> GetList() =>
            repo.GetAll().Select(x => ToDto(x));

        virtual public TDto GetById(TKey id)
        {
            var res = repo.Get(id);

            if(res != null)
                return ToDto(res);
            else
                throw NotFoundEx(id);
        }

        virtual public TDto CreateNew(TDto item)
        {
            var newEntity = FromDto(item);

            repo.Insert(newEntity);

            return ToDto(newEntity);
        }

        virtual public TDto UpdateById(TKey id, TDto item)
        {
            var o = repo.Get(id);

            if (o == null)
                throw NotFoundEx(id);

            var newE = FromDto(item, o);

            newE.Id = id;
            repo.Update(newE);

            return ToDto(newE);
        }

        virtual public void DeleteById(TKey id)
        {
            if (repo.Get(id) != null)
                repo.Delete(id);
            else
                throw NotFoundEx(id);
        }

        protected ArgumentException NotFoundEx(TKey id) =>
            new ArgumentException($"Entity {typeof(T).Name} with id <{id}> not found", "id");
    }
}
