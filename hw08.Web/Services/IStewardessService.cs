using hw08.Dto;
using hw08.Models;

namespace hw08.Services
{
    public interface IStewardessService : IAirService<long, Stewardess, StewardessDto> {}
}
