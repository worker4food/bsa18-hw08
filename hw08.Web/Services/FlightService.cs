using System;
using System.Linq;
using hw08.Dto;
using hw08.Models;
using hw08.DAL;

using TicketKind = hw08.Dto.TicketPrefDto.KindEnum;

namespace hw08.Services
{
    public interface IFlightService: IAirService<string, Flight, FlightDto> {
        FlightDto CreateNew(FlightSchedDto p);
        TicketDto BuyTicket(string flightId, TicketPrefDto opt);
        void ReturnTicket(string flightId, TicketDto ticket);

    }

    public class FlightService: DummyAirService<string, Flight, FlightDto>, IFlightService
    {
        protected IRepository<long, Ticket> ticketRepo;
        public FlightService(
            IRepository<string, Flight> r,
            IRepository<long, Ticket> ticketRepo,
            MapperService m) : base(r, m)
        {
            this.ticketRepo = ticketRepo;
        }

        public FlightDto CreateNew(FlightSchedDto flightSched)
        {
            var newId = Guid.NewGuid().GetHashCode().ToString("x").PadLeft(8, '0');
            var newF = FromDto(flightSched.Flight);

            newF.Tickets = flightSched.TicketParams
                .SelectMany(x =>
                    Enumerable.Range(1, x.Count ?? 0)
                    .Select(_ => new Ticket {
                        Flight = newF,
                        Price = x.Price ?? 0
                    }))
                    .ToList();

            newF.Id = newId;
            repo.Insert(newF);

            return ToDto(newF);
        }

        public TicketDto BuyTicket(string flightId, TicketPrefDto opt)
        {
            var tickets = ticketRepo.GetAll()
                .Where(x => x.Flight.Id == flightId);

            if(opt.Kind == TicketKind.MaxPriceEnum)
                tickets = tickets.OrderByDescending(x => x.Price);
            else if(opt.Kind == TicketKind.MinPriceEnum)
                tickets = tickets.OrderBy(x => x.Price);

            var res = tickets.First();

            ticketRepo.Delete(res.Id);

            return reverseMapper.Map<Ticket, TicketDto>(res);
        }

        public void ReturnTicket(string flightId, TicketDto ticket)
        {
            ticket.FlightId = flightId;
            var newTicket = directMapper.Map<TicketDto, Ticket>(ticket);

            ticketRepo.Insert(newTicket);
        }
    }
}
