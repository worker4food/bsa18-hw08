using hw08.Dto;
using hw08.Models;
using hw08.DAL;

namespace hw08.Services
{
    public class PilotService : DummyAirService<long, Pilot, PilotDto>, IPilotService
    {
        public PilotService(IRepository<long, Pilot> repo, MapperService m) : base(repo, m)
        {
        }
    }
}
