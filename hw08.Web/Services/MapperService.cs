using System.Linq;
using AutoMapper;
using hw08.Models;
using hw08.Dto;
using hw08.DAL;

namespace hw08.Services
{
    public class MapperService
    {
        public IMapper DirectMapper { get; }
        public IMapper ReverseMapper { get; }
        public MapperService(AirportEfDb db)
        {
            DirectMapper = new MapperConfiguration(cfg => {
                cfg.CreateMap<PlaneDto, Plane>()
                    .ForMember(
                        p => p.PlaneType,
                        opt => opt.MapFrom(src =>
                            db.Set<PlaneType>().Find(src.PlaneTypeId)));

                cfg.CreateMap<CrewDto, Crew>()
                    .ForMember(dest => dest.Pilot,
                        opt => opt.MapFrom(src =>
                            db.Set<Pilot>().Find(src.PilotId)))
                    .ForMember(dest => dest.CrewStewardesses,
                        opt => opt.MapFrom(src =>
                            src.Stewardesses.Select(s =>
                                new { Id = 0, Crew = src, Stewardess = s })));

                cfg.CreateMap<TicketDto, Ticket>()
                    .ForMember(dest => dest.Flight,
                        opt => opt.MapFrom(src =>
                            db.Set<Flight>().Find(src.FlightId)));

                cfg.CreateMap<DepartureDto, Departure>()
                    .ForMember(dest => dest.Crew,
                        opt => opt.MapFrom(src =>
                            db.Set<Crew>().Find(src.CrewId)))
                    .ForMember(dest => dest.Flight,
                        opt => opt.MapFrom(src =>
                            db.Set<Flight>().Find(src.FlightId)))
                    .ForMember(dest => dest.Plane,
                        opt => opt.MapFrom(src =>
                            db.Set<Plane>().Find(src.PlaneId)));

                cfg.CreateMap<PilotDto, Pilot>()
                    .ForMember(dest => dest.Crew,
                        opt => opt.Ignore());

                cfg.CreateMap<StewardessDto, Stewardess>()
                    .ForMember(dest => dest.CrewStewardesses,
                        o => o.Ignore());
            })
            .CreateMapper();

            ReverseMapper = new MapperConfiguration(cfg => {
                cfg.CreateMap<Crew, CrewDto>()
                    .ForMember(
                        dest => dest.Stewardesses,
                        opt => opt.MapFrom(src =>
                            src.CrewStewardesses.Select(x => x.Stewardess))
                    )
                    // .ForMember(
                    //     dest => dest.PilotId,
                    //     opt => opt.MapFrom(src => src.Pilot.Id)
                    // )
                    ;
            })
            .CreateMapper();
        }
    }
}
