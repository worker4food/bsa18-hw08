using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using hw08.DAL;
using hw08.Models;

namespace hw08.Services
{
    public interface IAirService<TKey, T, TDto>
        where T : Entity<TKey>
    {
        TDto ToDto(T x);

        T FromDto(TDto x);

        IEnumerable<TDto> GetList();

        TDto GetById(TKey id);

        TDto CreateNew(TDto item);

        TDto UpdateById(TKey id, TDto item);

        void DeleteById(TKey id);

    }
}
