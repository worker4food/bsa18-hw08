using System;
using System.Linq;
using hw08.Dto;
using hw08.Models;
using hw08.DAL;

namespace hw08.Services
{
    public class PlaneService: DummyAirService<long, Plane, PlaneDto>, IPlaneService
    {
        public PlaneService(IRepository<long, Plane> r, MapperService m) : base(r, m)
        {}
    }
}
