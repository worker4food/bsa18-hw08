using System.Collections.Generic;
using hw08.Dto;
using hw08.Models;

namespace hw08.Services
{
    public interface ICrewService : IAirService<long, Crew, CrewDto>
    {
        IEnumerable<StewardessDto> GetStewardessList(long id);
        StewardessDto AddStewardess(long crewId, long id);
        StewardessDto RemoveStewardess(long crewId, long id);
    }
}
