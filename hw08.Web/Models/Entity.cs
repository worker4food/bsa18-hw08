using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hw08.Models
{
    /// <summary>
    /// Base class for BL data
    /// </summary>
    public class Entity<T>
    {
        /// <summary>
        /// Entity unique Id
        /// </summary>
        public T Id { get; set; }
    }
}
