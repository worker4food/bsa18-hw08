using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using hw08.Models;
using hw08.Utils;

namespace hw08.DAL
{
    public class AirportEfDb : DbContext
    {
        public DbSet<PlaneType> PlaneType { get; set; }
        public DbSet<Plane> Plane { get; set; }
        public DbSet<Pilot> Pilot { get; set; }
        public DbSet<Stewardess> Stewardess { get; set; }
        public DbSet<Crew> Crew { get; set; }
        public DbSet<Flight> Flight { get; set; }
        public DbSet<Departure> Departure { get; set; }
        public DbSet<Ticket> Ticket { get; set; }
        public DbSet<CrewStewardess> CrewStewardess { get; set; }

        public AirportEfDb() : base()
        {
            if(Database.EnsureCreated())
                this.SeedData();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlite("Data Source=airport.db");
            //builder.EnableSensitiveDataLogging(true);
            base.OnConfiguring(builder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Crew>()
                .HasOne<Pilot>(x => x.Pilot)
                .WithOne(x => x.Crew)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Crew>()
                .HasMany<CrewStewardess>(x => x.CrewStewardesses)
                .WithOne(x => x.Crew)
                .HasForeignKey(x => x.StewardessId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CrewStewardess>()
                .HasOne<Crew>(x => x.Crew)
                .WithMany(x => x.CrewStewardesses)
                .HasForeignKey(x => x.CrewId);

            modelBuilder.Entity<CrewStewardess>()
                .HasOne<Stewardess>(x => x.Stewardess)
                .WithMany(x => x.CrewStewardesses)
                .HasForeignKey(x => x.StewardessId);
        }
    }
}
